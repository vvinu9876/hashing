 function openNotification(message,title){
        type = ['', 'info', 'danger', 'success', 'warning', 'rose', 'primary'];
        color = Math.floor((Math.random() * 6) + 1);

        $.notify({
            icon: "add_alert",
            message: "<b>"+title+"</b><p>"+message+"</p>"

        }, {
            type: type[color],
            timer: 3000,
            placement: {
            from: 'top',
            align: 'center'
          }
        });
      }