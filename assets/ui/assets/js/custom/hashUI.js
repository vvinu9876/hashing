var start_section = "<div class='row m-5'>";
var end_section = "</div>"

function getBucketComp(id){
    return  `<div class="col-md-2" style="border-color:red;border-width: 5px;border-style:dashed;">
                <center><h3  style="font-size: times;font-weight: bold;">B - `+id+`</h3></center>
            </div>`;
}

function getDataComp(value){
    return `<div id="col-md-1 col-lg-1">
                <center><h1 class="mt-2 ml-1">---></h1></center>
            </div>
            <div class="col-md-2" style="border-color:#9c27b0;border-width: 5px;border-style:solid;">
                <center><h6 class="mt-4" style="font-weight: normal;">`+value+`</h6></center>
            </div>
        `;
                    
}
        

function setTableHashTableJSON(json_hash_table){
            setTabContent("Loading...");
            var hash_table = JSON.parse(json_hash_table);
            var keys = Object.keys(hash_table);
            var html_cont = "";
            keys.forEach(function(key){
                html_cont = html_cont + start_section + getBucketComp(key);
                var value_arr = hash_table[key];
                for(var i=0;i<value_arr.length;i++){
                    if((i+1)%4==0){ //if it is more than 3 give a fresh count please
                        html_cont = html_cont + end_section;
                        html_cont = html_cont + start_section + getBucketComp(key);
                    }
                    html_cont = html_cont + getDataComp(value_arr[i]);
                }
                html_cont = html_cont + end_section;
            })
            $("#content").html(html_cont);
}