var databasename_global;

$('#addTable').click(function(){
    try{
        $(".modal").modal('hide');
        var dbname = $('#dbname').val();
        databasename_global = dbname;
        var tablename = $('#tablename').val();
        var columns = $("#columns").val(); //send as comma seperated string only please
        createTable(dbname,tablename,columns);
    }
    catch(err){
        setTabContent(err.message);
    }
});


function deleteTableSupport(tablename){
    deleteTable(databasename_global,tablename);
}

function openTables(tablename){
    if(databasename_global===null || databasename_global==="" || databasename_global===undefined){
        openNotification("Invalid Database Name","TABLE MANAGER");
        return;
    }
    window.location.href = "./RecordTable.html?dbname="+databasename_global+"&tablename="+tablename;
}
    

function setDBTables(json_data){
    setTabContent("Got JSON DATA "+json_data);
    var table_data = JSON.parse(json_data);
        
    if(table_data.found===false || table_data.found==="false"){
        setTabContent("No Database Found");
        return;
    }
    var headers = table_data.headers;
    var data_array = table_data.values;

    if(data_array.length===0){
        setTabContent("No Tables Found");
        return;
    }

    var table_html = `<table class="table">
                        <thead class=" text-primary">
                            <th>SL NO </th>
                            <th>Table Name</th>
                            <th>No. of Columns</th>
                            <th>Create Date</th>
                            <th>View</th>
                            <th>Delete</th>
                        </thead>
                    <tbody>`;
    var html_rows = "";
          
    for(var i=0;i<data_array.length;i++){
        html_rows = html_rows + "<tr> ";
        html_rows = html_rows +"<td>"+(i+1)+"</td>"; // add SL NO
            
        for(var j=0;j<headers.length;j++)
            html_rows = html_rows + "<td>" + data_array[i][headers[j]] + "</td>";
            
        html_rows = html_rows + "<td><button style='color:blue' onclick='openTables(\""+data_array[i][headers[0]]+"\")'>View</button></td>";;
        html_rows = html_rows + "<td><button style='color:red' onclick='deleteTableSupport(\""+data_array[i][headers[0]]+"\")'>Delete</button></td>";
        
        html_rows = html_rows+"</tr>";
    }
    table_html = table_html + html_rows + "</tbody></table>";
    $("#content").html(table_html);
        
}

$("#submit_button").click(function(){
    var dbname = $("#databasename").val();
    databasename_global = dbname;
    setTabContent("I got Called");
    getDatabaseTables(dbname);
})

$("#viewhashtable").click(function(){
    window.location.href = "./DBTablesHashTable.html?dbname="+databasename_global;
})

      

$(document).ready(function(){
    var url_string = window.location.href;
    var url = new URL(url_string);
    var dbname = url.searchParams.get("dbname");
    if(dbname==undefined || dbname===null || dbname===""){
        setTabContent("Enter DB NAME");
        return;
    }
    databasename_global = dbname;
    getDatabaseTables(dbname);
});