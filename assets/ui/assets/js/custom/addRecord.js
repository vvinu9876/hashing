var columns; //global variable 

function getInputHTML(id){
    var inp_html =` <div class="col-md-3">
                        <div class="form-group">
                            <label class="bmd-label-floating">`+id+`</label>
                        <input type="text" class="form-control" id="`+("supernova"+id)+`">
                    </div>
                    </div>
                    `;
    return inp_html;
}
      
      
function setHeaders(column_array){
    $("#add_record").show();
    var inp_html = "";
    try{
        
        var array = JSON.parse(column_array);
        
        if(array.length==0){
          setTabContent("No Table Found");
          $("#add_record").hide();
          return;
        }

        columns = array;
        array.forEach(function(column){
            inp_html = inp_html + getInputHTML(column);
        })

        $("#content").html(inp_html);
    }
    catch(err){
        $("#content").html("<h1>"+err.message+"</h1>");
    }
}


$("#submit_button").click(function(){
    var dbname = $("#databasename").val();
    var tablename = $("#tablename").val();
    getRecordHeaders(dbname,tablename);
})

$("#add_record").click(function(){
    var values = "";
    var i = 0;
    columns.forEach(function(column){
            
        if(i>0){
            values = values + ",";
        }
        values =  values + $("#supernova"+column).val();
        i++;
    })
    var dbname = $("#databasename").val();
    var tablename = $("#tablename").val();
    addRecord(dbname,tablename,values);
})

$(document).ready(function(){
    setTabContent("Enter Database and Table name");
    $("#add_record").hide();
})