$('#addDb').click(function(){
    $(".modal").modal('hide');
    var name = $('#dbname').val();
    createDatabase(name);
});

function deleteDB(name){
    setTabContent("I got called");
    var sure = window.confirm("Are you Sure ?");
    if(sure){
        deleteDatabase(name);
    }
    setTabContent("ITs done");
}


function openTables(dname){
    window.location.href = "./DBtables.html?dbname="+dname;
}

function setDataInTable(string_json_array){
    setTabContent(string_json_array);
    if(string_json_array===null || string_json_array===undefined ){
        setTabContent("No Databases Yet....");
        return;
    }
    try{
          var database_json = JSON.parse(string_json_array);
          
          //setTabContent("JSON Database  = "+database_json);
          var data_array = database_json.values; // array of json objects
          var headers = database_json.headers; // array of header names
          if(data_array.length===0){ 
            throw new Error("No Databases Yet....");
          }
          var table_html = `<table class="table">
                              <thead class=" text-primary">
                                <th>SL NO </th>
                                <th>Database Name</th>
                                <th>No. of Tables</th>
                                <th>Create Date</th>
                                <th>View</th>
                                <th>Delete</th>
                              </thead>
                            <tbody>`;
          var html_rows = "";
          for(var i=0;i<data_array.length;i++){
            html_rows = html_rows + "<tr> ";
            html_rows = html_rows +"<td>"+(i+1)+"</td>"; // add SL NO
            for(var j=0;j<headers.length;j++)
                html_rows = html_rows + "<td>" + data_array[i][headers[j]] + "</td>";
            html_rows = html_rows + "<td><button stle='color:blue' onclick='openTables(\""+data_array[i][headers[0]]+"\")'>View</button></td>";
            html_rows = html_rows + "<td><button  style='color:red' onclick='deleteDatabase(\""+data_array[i][headers[0]]+"\")'>Delete</button></td>";
            html_rows = html_rows+"</tr>";
          }
          table_html = table_html + html_rows + "</tbody></table>";
          $("#content").html(table_html);
    }
    catch(err){
        setTabContent("<h3>"+err.message+"</h3>");
    }
      
}