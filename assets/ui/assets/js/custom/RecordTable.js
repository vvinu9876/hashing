var databasename_global;
var tablename_global;


function deleteTableSupport(key){
    var dbname = databasename_global;
    var tablename = tablename_global;
    deleteRecord(dbname,tablename,key);
}

function setRecordTable(json_data){
    setTabContent("Got JSON DATA "+json_data);
    var table_data = JSON.parse(json_data);
        
    if(table_data.found===false || table_data.found==="false"){
        setTabContent("No Table Found");
        return;
    }
        
    var headers = table_data.headers;
    var data_array = table_data.values;

    if(data_array.length==0){
        setTabContent("No Records Found");
        return;
    }
        
    var table_html = `<table class="table"><thead class="text-primary"><tr>`;
        
    headers.forEach(function(header){
        table_html = table_html + "<th>"+header+"</th>";
    })

    table_html = table_html  +"<th>Delete</th>";
    table_html = table_html + "</tr></thead><tbody>";
    var html_rows = "";
          
    for(var i=0;i<data_array.length;i++){
        html_rows = html_rows + "<tr> ";
            
        for(var j=0;j<headers.length;j++)
            html_rows = html_rows + "<td>" + data_array[i][headers[j]] + "</td>";
        
        html_rows = html_rows + "<td><button href='javascript:void(0)' style='color:red' onclick='deleteTableSupport(\""+data_array[i][headers[0]]+"\")'>Delete</a></td>";
        html_rows = html_rows+"</tr>";
    }
    table_html = table_html + html_rows + "</tbody></table>";
    $("#content").html(table_html);
        
}

$("#submit_button").click(function(){
    var dbname = $("#databasename").val();
    var tablename = $("#tablename").val();
    databasename_global = dbname;
    tablename_global = tablename;
    setTabContent("I got Called");
    getRecordValues(dbname,tablename);
})

$("#viewhashtable").click(function(){
    var dbname = databasename_global;
    var tablename = tablename_global;
        
    if(dbname==null || dbname===undefined || dbname===""){
        openNotification("Please Enter DB NAME"," RECORD MANAGER");
        return;
    }
    
    if(tablename==null || tablename===undefined || tablename===""){
        openNotification("Please Enter TABLE NAME"," RECORD MANAGER");
        return;
    }
    window.location.href = "./RecordHashTable.html?dbname="+dbname+"&tablename="+tablename;
})

      

$(document).ready(function(){
    var url_string = window.location.href;
    var url = new URL(url_string);
    var dbname = url.searchParams.get("dbname");
    var tablename = url.searchParams.get("tablename");
    if(dbname==undefined || dbname===null || dbname==="" || tablename===undefined || tablename===null || tablename===""){
        setTabContent("Enter DB and Table NAME");
        return;
    }
    databasename_global = dbname;
    tablename_global = tablename;
    getRecordValues(dbname,tablename);
});