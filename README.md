# SUPERNOVA

# RECORD MANAGEMENT USING HASHING

## LIBRARIES USED
1. [ultralight-ux](https://github.com/ultralight-ux/ultralight-quick-start)
2. [MATERIAL-DESIGN](https://github.com/creativetimofficial/material-dashboard)



## 1. Install the Prerequisites

Before you build and run, you'll need to [install the prerequisites](https://docs.ultralig.ht/docs/installing-prerequisites) for your platform.

## 2. Clone and build the app

To clone the repo and build, run the following:

```shell
git clone git clone https://vvinu9876@bitbucket.org/vvinu9876/hashing.git
cd hashing
mkdir build
cd build
cmake ..
cmake --build . --config Release
```

> **Note**: _To force CMake to generate 64-SUPERNOVA projects on Windows, use `cmake .. -DCMAKE_GENERATOR_PLATFORM=x64` instead of `cmake ..`_

## 3. Run the app

### On macOS and Linux

Navigate to `hashing/build` and run `MyApp` to launch the program.

### On Windows

Navigate to `hashing/build/Release` and run `MyApp` to launch the program.

## DEVELOPED BY

#### VINAY P
#### vinayrasal7@gmail.com
