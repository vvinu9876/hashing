#include<iostream>
#include<string>
#include<list>
#include<fstream> 
#include <chrono>
#include <ctime>
#include "DBManager.cpp"
#include "hashing.cpp"
#include <nlohmann/json.hpp>
#include "utilityfunctions.cpp"
#pragma once 

using json = nlohmann::json;
using namespace std;

class TableManager{
public:
     string dbname;
     string globalpath = "/home/vinay/Projects/hashing/storage/";
     string filepath;
     string headers[3] = {"name","no_of_fields","create_date"};
     TableManager(string name);
     string createTable(string tablename,string headers[],int n);
     bool checkIfTableExists(string tablename);
     void addTableHeader(string tablename,string headers[]);
     string getTableListAsJSONString();
     string getHashTableAsJSON();
     string deleteTable(string tablename);
     int deleteTableHeader(string tablename);
};

TableManager::TableManager(string name){
     this->dbname = name;
     this->filepath = this->globalpath + name + ".txt";
     fstream f(filepath,ios::app); //just create the file if doesnt exists
     f.close();
}


void TableManager::addTableHeader(string tablename,string headers_list[]){
     string fileloc = globalpath+dbname+tablename+"_headers.txt";
     fstream f(fileloc,ios::app); // create if it doesnt exists
     f.close();
     Hash h(7,fileloc,headers_list,3);
     h.insertItem(headers_list);
}

int TableManager::deleteTableHeader(string tablename){
     string fileloc = globalpath+dbname+tablename+"_headers.txt";
     return remove(fileloc.c_str());
}


string TableManager::createTable(string tablename,string tableheaders[],int n){
    Hash h(7,filepath,headers,3);
    DBManager dba;
    auto time = chrono::system_clock::now();
    time_t date_time= chrono::system_clock::to_time_t(time);
    string table_data[3] = {tablename,to_string(n),ctime(&date_time)};
    if(h.checkIfKeyExists(tablename)){
         return "Table with this name already exists";
    }
    dba.changeNumberOfTables(dbname,1);
    h.insertItem(table_data);
    addTableHeader(tablename,tableheaders);
    return "Table created succesfully";
}

string TableManager::getTableListAsJSONString(){
     //check if database exists
     return getFileContentAsJSON(filepath,headers,3);
}


string TableManager::getHashTableAsJSON(){
     Hash h(7,filepath,headers,3);
     return h.getHashTableAsJSON().dump();
}

string TableManager::deleteTable(string tablename){
     Hash h(7,filepath,headers,3);
     DBManager dba;
     cout<<"DELETING ONE FROM DBNAME = "<<this->dbname<<endl;
     dba.changeNumberOfTables(dbname,-1);
     int status = deleteTableHeader(tablename);
     cout<<((status==0)?"File Deleted ":"Delete Failed");
     cout.flush();
     return h.deleteRecord(tablename);
}
