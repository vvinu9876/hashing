// CPP program to implement hashing with chaining 
#include<iostream> 
#include "utilityfunctions.cpp"
#include<map>
#include<list>
#include<utility>
#include <nlohmann/json.hpp>
#include<fstream>  
#pragma once 

using namespace std;
using json = nlohmann::json;

/*
##################################################
STRUCTURE 

[
    {
        "key" : {
                    "key" : "value",
                    "field" : "value",
                }
    }
]

*/



class Hash 
{ 
	int BUCKET; // No. of buckets 

    string filename;

    string headers[50];

    int headerlength;

	// Pointer to an array containing buckets 
	list<map<string,map<string,string>>> *table; 
public: 
	Hash(int V,string filename,string headernames[],int n); // Constructor 

	// inserts a key into hash table 
	void insertItem(string values[]); 

    void insertIntoHashTable(string key,string values[]);

	// deletes a key from hash table 
	void deleteItem(map<string,string> record); 

	// hash function to map values to key 
	int hashFunction(int x) { 
		return (x % BUCKET); 
	} 

    // retrieves the data from the record matching key
    map<string,list<map<string,string>>> retrieve(string key);

    // to clear hash table 
    void clearHashTable();

    // load content from file and create hash table
    void loadHashTableFromFile();

    // display hash table
	void displayHash(); 

    // return hash table as json 
    json getHashTableAsJSON();

    // check if key exists
    bool checkIfKeyExists(string key);

    //deleteARecordWithKey
    string deleteRecord(string key);
};





Hash::Hash(int b,string filename,string headernames[],int n) 
{   
    //set bucket size
	this->BUCKET = b;
    //set filename 
    this->filename = filename;
    //set no of headings 
    this->headerlength = n;
    //copy headernames to headers
    for(int i=0;i<n;i++)
        this->headers[i] = headernames[i];
    // create a table of bucket size rows with unlimited lists
	table = new list<map<string,map<string,string>>>[BUCKET]; 
} 


void Hash::insertItem(string values[]) 
{ 
    //open file
    fstream file(filename,ios::app);
    string buf;
    //convert the values to string
    for(int i=0;i<headerlength;i++){
        if(i!=headerlength-1) // add | only till last-1 node
            buf = buf + values[i] + "|";
        else
            buf = buf + values[i];
    }
    cout<<buf<<endl;
    cout.flush();
    //push buf string into the file
    file<<buf<<endl;
    // never remove/modify this code it might mess up adding an empty line after inserting
    file.close();
} 

void Hash::insertIntoHashTable(string key,string values[]){

    map<string,string> data;
    map<string,map<string,string>> row; 
    //Create row values as map
    for(int i=0;i<headerlength;i++){
        data.insert(pair<string,string>(headers[i],values[i]));
    }
	int index = hashFunction(getStringValue(key));
    //create a row
    row.insert(pair<string,map<string,string>>(key,data));
    //push into its table
    table[index].push_back(row);
    
}

//map<found(true or false), list<key,value> sorry i m using it in a confusing way.
// The first string is not key it is a flag
map<string,list<map<string,string>>> Hash::retrieve(string key){
    clearHashTable();
    loadHashTableFromFile();

    list<map<string,string>> matchingrecords;
    string found = "false";

    int index = hashFunction(getStringValue(key));

    list<map<string,map<string,string>>> records = table[index];
  
    for(auto x : records){
        map<string, map<string,string>>::iterator itr;
        for(itr =  x.begin() ; itr != x.end() ; ++itr)// there will be only one element { key : {field:value,field,value} }
            if(itr->first == key){ // make sure key matches
                found = "true";
                matchingrecords.push_back(itr->second);
            }
    }
       
    map<string,list<map<string,string>>> returnrecord;
    returnrecord.insert(pair<string,list<map<string,string>>>(found,matchingrecords));
    return returnrecord;
} 

bool Hash::checkIfKeyExists(string key){
    clearHashTable();
    loadHashTableFromFile();

    cout<<"I got called "<<key<<endl;

    for (int i = 0; i < BUCKET; i++) { 
        cout<<" i = "<<i<<endl;
        cout.flush();
        for (auto x : table[i]){
            map<string, map<string,string>>::iterator itr;
            for(itr =  x.begin() ; itr != x.end() ; ++itr) // there will be only one element { key(first) : (second){field:value,field,value} }
                if(itr->first==key){
                    cout<<" KEy = "<<itr->first<<endl;
                    return true; 
                }
        }   
    }

    return false;
}


string Hash::deleteRecord(string key){
    map<string,list<map<string,string>>> result = retrieve(key);
    map<string,string> record_to_delete;
    if(result.begin()->first=="true"){ //if found
        int i = 1 ; 
        for(auto x : result.begin()->second){
            if(i>1){ // if there are more records with same data , delete only one
                break;
            }
            record_to_delete = x;
            i++;
        }
        deleteItem(record_to_delete);
        return "Database Deleted Succesfully";
    }
    return "Database Not Found";
}


void Hash::deleteItem(map<string,string> record) { 
    // open file please
    ifstream inpfile(filename);
    // temp.txt stores all the lines except that is to be deleted
    fstream tempfile("temp.txt",ios::app);

    string fileline;
    
    string recordline = "";

    for(int i=0;i<headerlength;i++){
        if(i!=headerlength-1)    
            recordline = recordline + record[headers[i]] + "|" ;
        else
            recordline = recordline + record[headers[i]];
    }
    
    while(!inpfile.eof()){
        getline(inpfile,fileline);
        if(fileline!=recordline){
            if(fileline.length()>0)// donot allow empty lines will \n matter
                tempfile<<fileline<<endl;
        }
    }

    inpfile.close();
    
    tempfile.close();
    // remove the file
    remove(filename.c_str());
    // rename the file
    rename("temp.txt",filename.c_str());
    
}

void Hash::clearHashTable(){
    for(int i=0 ; i < BUCKET ; i++){
        table[i].clear();
    }
}

void Hash::loadHashTableFromFile(){
    fstream file(filename,ios::in); 
    string line;
    list<string> row;
    string values[headerlength];
    int i=0;
    while(!file.eof()){
        getline(file,line); // get line
        if(line.length()==0){ // you either breaking this loop when it reads empty line or break you *** 
            continue;
        }
        row = splitString(line,'|'); //split string and get all values into array
        string key = row.front(); // get the key
        int i = 0;
        for(auto val : row){
            values[i] = val ;
            i = i + 1;
        }
        insertIntoHashTable(key,values);
    }
    file.close(); 
}


// function to display hash table 
void Hash::displayHash() {
    
    clearHashTable();
    loadHashTableFromFile();
    
    for (int i = 0; i < BUCKET; i++) { 
	    std::cout << i; 
	    for (auto x : table[i]){
            map<string, map<string,string>>::iterator itr;
            for(itr =  x.begin() ; itr != x.end() ; ++itr) // there will be only one element { key(first) : (second){field:value,field,value} }
                std::cout << " --> " << itr->first;  
        }   
	    std::cout << endl; 
    }
} 

json Hash::getHashTableAsJSON(){
    clearHashTable();
    loadHashTableFromFile();
    map<string, map<string,string>>::iterator itr;
    json hash_table;
    json array = json::array();
    for (int i = 0; i < BUCKET; i++) {
        array.clear();
        for(auto x : table[i]){
            for(itr =  x.begin() ; itr != x.end() ; ++itr){
                array.push_back(itr->first); //push all keys
            }
        }
        hash_table[to_string(i)] = array;
    } 
    return hash_table;
}


void handleRetreive(Hash obj){
    string key;
    std::cout<<"Enter the key:\t";
    cin>>key;
    map<string,list<map<string,string>>> result;
    list<map<string,string>> records;
    result = obj.retrieve(key);
    if(result.begin()->first=="true"){
        map<string,string>::iterator itr;
        records = result.begin()->second;
        for(auto record : records)
            for(itr =  record.begin() ; itr != record.end() ; ++itr)
                std::cout << " Key =  "<<itr->first<<" Value = "<< itr->second<<endl;
    }
    else{
       std::cout<<endl<<"REcord Not Found";
    }

}

void handleDelete(Hash h){
    string key;
    std::cout<<"Enter the key:\t";
    cin>>key;
    map<string,list<map<string,string>>> result;
    list<map<string,string>> records;
    result = h.retrieve(key);

    if(result.begin()->first=="true"){
        for(auto x : result.begin()->second){
            h.deleteItem(x);
            cout<<"Done";
        }
    }

}

/*
// Driver program 
int main() 
{ 
    string filename = "sample.txt";
    string headernames[2] = {"key","val"};
    string values[2] = {"hello","world"};
    int size = 2;
    
    std::cout << " Adding values";
    // insert the keys into the hash table 
    Hash h(7,filename,headernames,size); // 7 is count of buckets in 
			// hash table 
    int n =2;

    int ch ;

    do{
        std::cout<<endl<<"Select An Value"<<endl;
        cin >> ch;
        switch(ch){
            case 1 : h.insertItem(values); break;
            case 2 : h.displayHash();break;
            case 3 : handleRetreive(h);break;
            case 4 : handleDelete(h);break;
            case 5 : exit(0);break;
            default : continue;break;
        }
        std::cout<<endl;
    }
    while(ch!=5);

    return 1;
} 

*/

