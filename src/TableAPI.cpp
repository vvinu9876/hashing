#include<iostream>
#include<string>
#include<list> 
#include <JavaScriptCore/JavaScript.h>
#include "TableManager.cpp"
#include "convertJSStringToString.cpp"
#pragma once

using namespace std;
using json = nlohmann::json;

JSValueRef createTable(JSContextRef ctx, JSObjectRef function, JSObjectRef thisObject, size_t argumentCount, 
                        const JSValueRef arguments[], JSValueRef* exception){

    string dbname = JSStringToStdString(JSValueToStringCopy(ctx,arguments[0],exception));
    string tablename = JSStringToStdString(JSValueToStringCopy(ctx,arguments[1],exception));
    string column_names = JSStringToStdString(JSValueToStringCopy(ctx,arguments[2],exception)); //comma seperated column name
    list<string> columns = splitString(column_names,',');
    TableManager tba(dbname);
    string headers[columns.size()];
    int i = 0;
    for(auto x : columns)
        headers[i++] = x;
    string result;
    try{
        result = tba.createTable(tablename,headers,columns.size());
    }
    catch(const char* msg){
        cout<<"MEssage = "<<msg<<endl;
        cout.flush();
    }
    string to_notify = "openNotification(' "+result+" ', 'DB Table Manager')";
    modifyHTML(ctx,to_notify.c_str());
    return JSValueMakeNull(ctx);     
}

JSValueRef getDatabaseTables(JSContextRef ctx, JSObjectRef function, JSObjectRef thisObject, size_t argumentCount, 
                        const JSValueRef arguments[], JSValueRef* exception){
    string dbname = JSStringToStdString(JSValueToStringCopy(ctx,arguments[0],exception));
    TableManager tbm(dbname);
    cout<<"DB NAME = "<<dbname<<endl;
    cout.flush();
    try{
    string result = tbm.getTableListAsJSONString();
    string to_dramitize = "setDBTables('"+result+"');";
    modifyHTML(ctx,to_dramitize.c_str());
    }
    catch(const char* message){
        cout<<" Exception "<<message<<endl;
        cout.flush();
    }
    return JSValueMakeNull(ctx);    
}

JSValueRef deleteTable(JSContextRef ctx, JSObjectRef function, JSObjectRef thisObject, size_t argumentCount, 
                        const JSValueRef arguments[], JSValueRef* exception){

        string dbname = JSStringToStdString(JSValueToStringCopy(ctx,arguments[0],exception));
        string tablename = JSStringToStdString(JSValueToStringCopy(ctx,arguments[1],exception));
        TableManager tba(dbname);
        string to_exec = "openNotification('"+tba.deleteTable(tablename)+" . Refresh to see changes','Table Manager')";
        modifyHTML(ctx,to_exec.c_str());
        return JSValueMakeNull(ctx);
}

JSValueRef getTableHashTable(JSContextRef ctx, JSObjectRef function, JSObjectRef thisObject, size_t argumentCount, 
                        const JSValueRef arguments[], JSValueRef* exception){
        
        string dbname = JSStringToStdString(JSValueToStringCopy(ctx,arguments[0],exception));
        TableManager tba(dbname);
        string hashtablejson = tba.getHashTableAsJSON();
        string to_exec = "setTableHashTableJSON('"+hashtablejson+"');";
        modifyHTML(ctx,to_exec.c_str());
        return JSValueMakeNull(ctx); 
}