#include<iostream> 
#include<string>
#include <JavaScriptCore/JavaScript.h>
#include "DBManager.cpp"
#include "convertJSStringToString.cpp"
#pragma once

using namespace std;


void setDBTable(DBManager dba,JSContextRef ctx){
    string to_set_table = "setDataInTable('"+dba.getDatabaseListAsJSONString()+"');";
    modifyHTML(ctx,to_set_table.c_str());
}

void setHashTable(DBManager dba,JSContextRef ctx){
    Hash h(7,dba.globalpath,dba.headers,3);
    string to_set_hashtable = "setTableHashTableJSON('"+h.getHashTableAsJSON().dump()+"')"; //send json as string please
    modifyHTML(ctx,to_set_hashtable.c_str());
}


JSValueRef createDatabase(JSContextRef ctx, JSObjectRef function, JSObjectRef thisObject, size_t argumentCount, 
                        const JSValueRef arguments[], JSValueRef* exception){

    
    DBManager dba; // lol its db admin
    //Get Parameters
    string dbname = JSStringToStdString(JSValueToStringCopy(ctx,arguments[0],exception));
    string result = dba.createDatabase(dbname);
    string to_notify = "openNotification(' "+result+" ', 'DB Manager')";
    modifyHTML(ctx,to_notify.c_str());
    //refresh items
    setDBTable(dba,ctx);
    //create database  
    return JSValueMakeNull(ctx);
}


JSValueRef setDatabaseTable(JSContextRef ctx, JSObjectRef function, JSObjectRef thisObject, size_t argumentCount, 
                        const JSValueRef arguments[], JSValueRef* exception){
    DBManager dba; 
    setDBTable(dba,ctx);
    return JSValueMakeNull(ctx);
}

JSValueRef setDatabaseHashTable(JSContextRef ctx, JSObjectRef function, JSObjectRef thisObject, size_t argumentCount, 
                        const JSValueRef arguments[], JSValueRef* exception){
    DBManager dba; 
    setHashTable(dba,ctx);
    return JSValueMakeNull(ctx);
}


JSValueRef deleteDatabase(JSContextRef ctx, JSObjectRef function, JSObjectRef thisObject, size_t argumentCount, 
                        const JSValueRef arguments[], JSValueRef* exception){
    DBManager dba;
    //Get Parameters
    string dbname = JSStringToStdString(JSValueToStringCopy(ctx,arguments[0],exception));
    string result = "openNotification('"+dba.deleteDatabase(dbname)+"','DB Manager')";
    modifyHTML(ctx,result.c_str());
    setDBTable(dba,ctx);
    return JSValueMakeNull(ctx);
}

