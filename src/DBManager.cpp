#include<iostream> 
#include<string>
#include<list>
#include <nlohmann/json.hpp>
#include <chrono>
#include <ctime> 
#include "hashing.cpp"
#pragma once

using json = nlohmann::json;
using namespace std;

class DBManager{
    
public:
    string globalpath = "/home/vinay/Projects/hashing/storage/databases.txt";
    string headers[3] = {"dbname","no_of_tables","create_date_time"};
    string createDatabase(string name);
    string getDatabaseListAsJSONString();
    string deleteDatabase(string dbname);
    void changeNumberOfTables(string dbname,int val); //either plus or minus
};

string DBManager::getDatabaseListAsJSONString(){
    return getFileContentAsJSON(globalpath,headers,3);
     // return stringified json
}   


string DBManager::createDatabase(string name){
    Hash h(7,globalpath,headers,3);
    auto time = chrono::system_clock::now();
    time_t date_time= chrono::system_clock::to_time_t(time);
    string dbdata[3] = {name,"0",ctime(&date_time)};
    // Name ,  no_of_tables , create_date_time 
    if(h.checkIfKeyExists(name))
        return "Database with that name Already Exists";
    h.insertItem(dbdata);
    return "Database Added Succesfully";
}

void DBManager::changeNumberOfTables(string dbname,int val){
    Hash h(7,globalpath,headers,3);
    map<string,list<map<string,string>>> result = h.retrieve(dbname);
    map<string,string> record;
    string values[3];
    if(result.begin()->first=="true"){
        map<string,string>::iterator itr;
        for(auto x : result.begin()->second)
            record = x;
        for(int i=0;i<3;i++){
            if(headers[i]=="no_of_tables")
                values[i] = to_string(stoi(record[headers[i]]) + val); // convert to int and sum the value
            else
                values[i] = record[headers[i]];
        }
    }
    h.deleteItem(record);
    h.insertItem(values);
}

string DBManager::deleteDatabase(string dbname){
    Hash h(7,globalpath,headers,3);
    return h.deleteRecord(dbname);
}





