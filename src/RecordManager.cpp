#include<iostream>
#include<list>
#include<string>
#include<fstream> 
#include <chrono>
#include <ctime>
#include "DBManager.cpp"
#include "hashing.cpp"
#include <nlohmann/json.hpp>
#include "utilityfunctions.cpp"
#pragma once 

using json = nlohmann::json;
using namespace std;

class RecordManager{
public:
    string dbname;
    string tablename;
    string globalpath = "/home/vinay/Projects/hashing/storage/";
    string tablevalueloc;
    int headerlength;
    string headers[100];
    RecordManager(string dbname,string tablename);
    void addRecord(string values[]);
    string deleteRecord(string key);
    string getTableValuesAsJSON();
    string getHashTable();
};

RecordManager::RecordManager(string databasename,string dbtablename){
    this->dbname = databasename;
    this->tablename = dbtablename;
    this->tablevalueloc = this->globalpath + dbname + tablename + "_values.txt";
    fstream file(this->tablevalueloc,ios::app); //just create file if it doesnt exists
    file.close(); 
    fstream headerfile(this->globalpath + dbname + tablename + "_headers.txt");
    string line;
    //get first line only
    getline(headerfile,line);
    list<string> recordheaders = splitString(line,'|');
    this->headerlength = recordheaders.size();
    int i = 0;
    for(auto x : recordheaders){
        this->headers[i++] = x;
    }
    headerfile.close();
}


void RecordManager::addRecord(string values[]){
    Hash h(7,tablevalueloc,headers,headerlength);
    h.insertItem(values);
}

string RecordManager::deleteRecord(string key){
    Hash h(7,tablevalueloc,headers,headerlength);
    return h.deleteRecord(key);
}

string RecordManager::getTableValuesAsJSON(){
    return getFileContentAsJSON(tablevalueloc,headers,headerlength); 
}

string RecordManager::getHashTable(){
    Hash h(7,tablevalueloc,headers,headerlength);
    return h.getHashTableAsJSON().dump();
}
