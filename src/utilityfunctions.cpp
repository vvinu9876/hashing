#include<iostream>
#include<list>
#include<fstream> 
#include<sstream>
#include <JavaScriptCore/JavaScript.h>
#pragma once


using namespace std;
using json = nlohmann::json;

list<string> splitString(string str,char delimiter){
	
	list<string> values;
	stringstream check1(str);
	string intermediate; 
    // Tokenizing w.r.t. space ' ' 
    while(getline(check1, intermediate, delimiter)) 
    { 
        values.push_back(intermediate); 
    } 
	return values;
}

int getStringValue(string key){
    int val = 0;
    for(int i=0;i<key.length();i++)
        val = val + key[i];
    return val;
}

void modifyHTML(JSContextRef ctx,const char* textscript){
    JSStringRef script = JSStringCreateWithUTF8CString(textscript);
    JSEvaluateScript(ctx, script, 0, 0, 0, 0);
    JSStringRelease(script);
}


string getFileContentAsJSON(string filepath,string headers[],int headerlength){
  
    ifstream file(filepath);
    string line;
    json to_return; // {headers : [header1,header2....headern],values : [{valuekey:value}..]}
    json data_array = json::array(); // its an json array [ {key:value},{key:value}]
    json row; // its an json object {key:value}
    if(!file){
        to_return["found"] = false;
        return to_return.dump();
    }
    json header_array = json::array();
    for(int index=0;index<headerlength;index++){
        header_array.push_back(headers[index]);
    }
    while(!file.eof()){
        getline(file,line);
        cout<<"File Line "<<line<<endl;
        cout.flush();
        if(line.length()==0){
            continue;
        }
        list<string> values = splitString(line,'|');
        int i = 0;
        for(auto x : values)
            row[headers[i++]] = x;
        //push the result json array
        data_array.push_back(row);
        //clear elements in the row
        row.clear();
    }
    to_return["found"] = "true";
    to_return["values"] = data_array;
    to_return["headers"] = header_array;
    file.close();
    return to_return.dump();
}


