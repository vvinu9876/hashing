#include<iostream>
#include<string>
#include<list> 
#include <JavaScriptCore/JavaScript.h>
#include "convertJSStringToString.cpp"
#include "RecordManager.cpp"

using namespace std;
using json = nlohmann::json;

JSValueRef addRecord(JSContextRef ctx, JSObjectRef function, JSObjectRef thisObject, size_t argumentCount, 
                        const JSValueRef arguments[], JSValueRef* exception){

    string dbname = JSStringToStdString(JSValueToStringCopy(ctx,arguments[0],exception));
    string tablename = JSStringToStdString(JSValueToStringCopy(ctx,arguments[1],exception));
    string values = JSStringToStdString(JSValueToStringCopy(ctx,arguments[2],exception)); 
    list<string> values_list = splitString(values,',');
    string value_arr[values_list.size()];
    int i=0;
    for(auto x : values_list){
        value_arr[i++] = x;
    }
    RecordManager rma(dbname,tablename);
    rma.addRecord(value_arr);
    string to_notify = "openNotification(' Succesfully Inserted ', 'Record Manager')";
    modifyHTML(ctx,to_notify.c_str());
    return JSValueMakeNull(ctx);
}

JSValueRef deleteRecord(JSContextRef ctx, JSObjectRef function, JSObjectRef thisObject, size_t argumentCount, 
                        const JSValueRef arguments[], JSValueRef* exception){
    string dbname = JSStringToStdString(JSValueToStringCopy(ctx,arguments[0],exception));
    string tablename = JSStringToStdString(JSValueToStringCopy(ctx,arguments[1],exception));
    string key = JSStringToStdString(JSValueToStringCopy(ctx,arguments[2],exception)); 

    RecordManager rma(dbname,tablename);
    string result  = rma.deleteRecord(key);
    string to_notify = "openNotification(' "+result+" Refresh to notice change', 'Record Manager')";
    modifyHTML(ctx,to_notify.c_str());
    return JSValueMakeNull(ctx);
}

JSValueRef getRecordHeaders(JSContextRef ctx, JSObjectRef function, JSObjectRef thisObject, size_t argumentCount, 
                        const JSValueRef arguments[], JSValueRef* exception){

    string dbname = JSStringToStdString(JSValueToStringCopy(ctx,arguments[0],exception));
    string tablename = JSStringToStdString(JSValueToStringCopy(ctx,arguments[1],exception));

    RecordManager rma(dbname,tablename);
    json header_array = json::array();
    for(int i=0;i<rma.headerlength;i++){
        header_array.push_back(rma.headers[i]);
    }
    cout<<"header_array = "<<header_array.dump()<<endl;
    cout.flush();
    string to_exec = " setHeaders(' "+header_array.dump()+" ' );";
    modifyHTML(ctx,to_exec.c_str());
    return JSValueMakeNull(ctx);
}


//return json string
JSValueRef getRecordValues(JSContextRef ctx, JSObjectRef function, JSObjectRef thisObject, size_t argumentCount, 
                        const JSValueRef arguments[], JSValueRef* exception){
            
    string dbname = JSStringToStdString(JSValueToStringCopy(ctx,arguments[0],exception));
    string tablename = JSStringToStdString(JSValueToStringCopy(ctx,arguments[1],exception));
    RecordManager rma(dbname,tablename);
    string result = rma.getTableValuesAsJSON();
    string to_exec = "setRecordTable( ' "+result+" ')";
    modifyHTML(ctx,to_exec.c_str());
    return JSValueMakeNull(ctx);
}

JSValueRef getRecordsHashTable(JSContextRef ctx, JSObjectRef function, JSObjectRef thisObject, size_t argumentCount, 
                        const JSValueRef arguments[], JSValueRef* exception){
    string dbname = JSStringToStdString(JSValueToStringCopy(ctx,arguments[0],exception));
    string tablename = JSStringToStdString(JSValueToStringCopy(ctx,arguments[1],exception));
    cout<<"DBNAME = "<<dbname<<endl;
    cout.flush();
    cout<<"TABLENAME ="<<tablename<<endl;
    cout.flush();
    RecordManager rma(dbname,tablename);
    cout<<"FILELOC = "<<rma.tablevalueloc<<endl;
    cout.flush();
    string result = rma.getHashTable();
    cout<<"RESULT = "<<result<<endl;
    cout.flush();
    string to_exec = " setTableHashTableJSON( ' "+result+" ' );";
    modifyHTML(ctx,to_exec.c_str());
    return JSValueMakeNull(ctx);
}

